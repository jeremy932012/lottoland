import typescript from 'rollup-plugin-typescript2';
import sass from 'rollup-plugin-sass';

export default {
    input: './src/Dropdown.tsx',
    output: {
        file: './build/Dropdown.js',
        format: 'cjs',
        name: 'dropdown'
    },
    plugins: [
        sass({insert: true}),
        typescript()
    ]
}
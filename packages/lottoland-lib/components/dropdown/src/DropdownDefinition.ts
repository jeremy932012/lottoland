export interface IDropdownProps {
    items: IDropdownItem[];
    selectedKey?: string;
    placeholder?: string;
    open?: boolean;
    onItemClick?: (item: IDropdownItem) => void;
}

export interface IDropdownItem {
    key: string;
    value: string;
}
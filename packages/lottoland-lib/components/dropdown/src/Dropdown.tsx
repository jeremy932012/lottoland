import React, { useEffect, useRef, useState } from "react";
import { IDropdownItem, IDropdownProps } from "./DropdownDefinition";
import { FaAngleDown, FaAngleUp } from "react-icons/fa"
import "./Dropdown.scss";

export function Dropdown(props: IDropdownProps) {
    const dropdownRef = useRef<HTMLDivElement>(null);
    const [selectedKey, setSelectedKey] = useState(props.selectedKey);
    const [isOpen, setOpen] = useState(props.open || false);

    const onLabelClick = (evt: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
        setOpen(!isOpen);
        evt.stopPropagation();
    }

    const onItemClick = (evt: React.MouseEvent<HTMLDivElement, MouseEvent>, item: IDropdownItem) => {
        setSelectedKey(item.key);
        setOpen(false);
        if(props.onItemClick) 
            props.onItemClick(item);
        evt.stopPropagation();
    }

    useEffect(() => {
        const handleOutsideClick = (event: MouseEvent) => {
            if(dropdownRef.current && !dropdownRef.current!.contains(event.target as Node)) {
                setOpen(false);
            }
        }
        window.addEventListener("click", handleOutsideClick);

        return () => window.removeEventListener("click", handleOutsideClick);
    })

    return (
        <div ref={dropdownRef} className="dropdown--container">
            <div onClick={onLabelClick} className={`dropdown--container__label ${isOpen ? "opened" : ""}`}>
                {selectedKey ? 
                    <span className="dropdown--label__selected">
                        {DropdownHelper(props.items).getValueFromKey(selectedKey)}
                    </span> 
                    :
                    <span className="dropdown--label__placeholder">{props.placeholder}</span>}

                <span className="dropdown--label__icon">
                    {isOpen ? <FaAngleUp /> : <FaAngleDown />}
                </span>
            </div>
            {isOpen && <div className="dropdown--container__content">
                {props.items.map(item => 
                <div onClick={(evt) => onItemClick(evt, item)} 
                className={`dropdown--content__item ${item.key == selectedKey ? "selected" : ""}`}>
                    {item.value}
                </div>)}
            </div>}
        </div>
    );
}

function DropdownHelper(items: IDropdownItem[]) {
    return {
        getValueFromKey: (key?: string) => getValueFromKey(items, key)
    }
}

function getValueFromKey(items: IDropdownItem[], key?: string) {
    const selectedItem = items.find(item => item.key == key);
    return selectedItem ? selectedItem.value : null;
}
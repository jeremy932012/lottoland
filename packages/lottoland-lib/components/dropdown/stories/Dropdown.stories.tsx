import React from 'react';
import { Dropdown } from '../src/Dropdown';
import { IDropdownProps } from '../src/DropdownDefinition';
import { Story } from '@storybook/react/types-6-0';

export default {
    title: 'Dropdown',
    component: Dropdown,
}

const Template: Story<IDropdownProps> = (args) => <Dropdown {...args} />;

const items = [
    {key: "hello", value: "Hello"},
    {key: "bye", value: "Bye"},
    {key: "goodMorning", value: "Good Morning"},
]

export const Primary = Template.bind({});
Primary.args = { items, placeholder: "Select one option" };
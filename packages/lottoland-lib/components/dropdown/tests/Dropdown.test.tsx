/**
 * @jest-environment jsdom
 */
import React from "react";
import { Dropdown } from "../src/Dropdown";
import { create } from 'react-test-renderer';
import { shallow } from 'enzyme';
import { IDropdownProps } from "../src/DropdownDefinition";

const items = [
    {key: "hello", value: "Hello"},
    {key: "bye", value: "Bye"},
    {key: "goodMorning", value: "Good Morning"},
]

const dropdownComponentTest = (props?: Partial<IDropdownProps>) => {
    const simpleDropdown = <Dropdown items={items} placeholder="Select" {...props} />;

    return {
        wrapper: shallow(simpleDropdown),
        snapshot: create(simpleDropdown)
    }
}

describe("Dropdown", () => {
    test("Simple Snapshot", () => {
        const dropdownComponent = dropdownComponentTest().snapshot;
        expect(dropdownComponent).toMatchSnapshot()
    }),
    test("Selected Value", () => {
        const dropdownComponent = dropdownComponentTest({selectedKey: items[0].key}).wrapper;
        const selectedValue = dropdownComponent.find(".dropdown--label__selected").text();
        expect(selectedValue).toEqual(items[0].value)
    })
})
import React from "react";
import { Header } from "../src/Header";
import { create } from 'react-test-renderer';
import { shallow } from 'enzyme';
import { IHeaderProps } from "../src/HeaderDefinition";

const headerComponentTest = (props?: Partial<IHeaderProps>) => {
    const simpleTable = <Header title={"Lottoland"} {...props} />;

    return {
        wrapper: shallow(simpleTable),
        snapshot: create(simpleTable)
    }
}

describe("Header", () => {
    test("Simple Snapshot", () => {
        const tableComponent = headerComponentTest().snapshot;
        expect(tableComponent).toMatchSnapshot()
    })
})
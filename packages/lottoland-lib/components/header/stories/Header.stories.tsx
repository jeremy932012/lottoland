import React from 'react';
import { Header } from '../src/Header';
import { IHeaderProps } from '../src/HeaderDefinition';
import { Story } from '@storybook/react/types-6-0';

export default {
    title: 'Header',
    component: Header,
}

const Template: Story<IHeaderProps> = (args) => <Header {...args} />;

export const Primary = Template.bind({});
Primary.args = { title: "Lottoland" };
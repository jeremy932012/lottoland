import React from "react";
import { IHeaderProps } from "./HeaderDefinition";
import "./Header.scss";

export function Header(props: IHeaderProps) {

    return <div className="header--container">
        <h1>{props.title}</h1>
    </div>;
}
module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    transform: {
      '^.+\\.tsx?$': 'ts-jest'
    },
    moduleNameMapper: {
        '\\.(css|scss)$': 'identity-obj-proxy'
    },
    testRegex: '/tests/.*\\.test.(ts|tsx)$',
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
    setupFiles: ['<rootDir>/setupTests.ts'],
    collectCoverage: true,
    collectCoverageFrom: [ "**/src/**", "!**/src/__tests__/**", "!**/node_modules/**" ]
  };
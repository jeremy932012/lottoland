import typescript from 'rollup-plugin-typescript2';
import sass from 'rollup-plugin-sass';

export default {
    input: './src/Header.tsx',
    output: {
        file: './build/Header.js',
        format: 'cjs',
        name: 'header'
    },
    plugins: [
        sass({insert: true}),
        typescript()
    ]
}
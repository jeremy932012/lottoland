import React from 'react';
import { Table } from '../src/Table';
import { ITableProps, ITableRow } from '../src/TableDefinition';
import { Story } from '@storybook/react/types-6-0';

export default {
    title: 'Table',
    component: Table,
}

const Template: Story<ITableProps> = (args) => <Table {...args} />;

const header = [
    {accessor: "hello", title: "Hello"},
    {accessor: "bye", title: "Bye"},
    {accessor: "goodMorning", title: "Good Morning"},
]

const data: ITableRow[] = [
    {
        hello: "Salut",
        bye: "Au revoir",
        goodMorning: "Bonjour"
    },
    {
        hello: "Hola",
        bye: "Adios",
        goodMorning: "Buenos dias"
    },
    {
        hello: "Hallo",
        bye: "Auf Wiedersehen",
        goodMorning: "Guten Morgen"
    }
]

export const Primary = Template.bind({});
Primary.args = { header, data };
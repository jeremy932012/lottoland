import React from "react";
import { Table } from "../src/Table";
import { create } from 'react-test-renderer';
import { shallow } from 'enzyme';
import { ITableProps, ITableRow } from "../src/TableDefinition";

const header = [
    {accessor: "hello", title: "Hello"},
    {accessor: "bye", title: "Bye"},
    {accessor: "goodMorning", title: "Good Morning"},
]

const data: ITableRow[] = [
    {
        hello: "Salut",
        bye: "Au revoir",
        goodMorning: "Bonjour"
    },
    {
        hello: "Hola",
        bye: "Adios",
        goodMorning: "Buenos dias"
    },
    {
        hello: "Hallo",
        bye: "Auf Wiedersehen",
        goodMorning: "Guten Morgen"
    }
]

const tableComponentTest = (props?: Partial<ITableProps>) => {
    const simpleTable = <Table header={header} data={data} {...props} />;

    return {
        wrapper: shallow(simpleTable),
        snapshot: create(simpleTable)
    }
}

describe("Table", () => {
    test("Simple Snapshot", () => {
        const tableComponent = tableComponentTest().snapshot;
        expect(tableComponent).toMatchSnapshot()
    })
})
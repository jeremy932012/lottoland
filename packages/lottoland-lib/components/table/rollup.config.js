import typescript from 'rollup-plugin-typescript2';
import sass from 'rollup-plugin-sass';

export default {
    input: './src/Table.tsx',
    output: {
        file: './build/Table.js',
        format: 'cjs',
        name: 'table'
    },
    plugins: [
        sass({insert: true}),
        typescript()
    ]
}
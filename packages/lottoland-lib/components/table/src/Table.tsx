import React from "react";
import { ITableProps } from "./TableDefinition";
import "./Table.scss";

export function Table(props: ITableProps) {
    const header = props.header.sort((a, b) => (a.order || 0) - (b.order || 0));

    return <div className="table--container">
        <div className="table--container__header">
            {header.map(h => <div key={h.accessor} className="table--header__title">{h.title}</div>)}
        </div>
        <div className="table--container__content">
            {props.data.map((row, i) => <div key={i} className="table--content__row">
                <div className="table--content__header">
                    {header.map(h => <div key={h.accessor+i} className="table--header__title">{h.title}</div>)}
                </div>
                <div className="table--content__data">
                    {header.map(h => <div key={h.accessor+"data"} className="table--content__cell">
                        {row[h.accessor]}
                    </div>)}
                </div>
            </div>)}
        </div>
    </div>;
}
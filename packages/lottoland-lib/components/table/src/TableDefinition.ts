export interface ITableProps {
    header: ITableHeader[],
    data: ITableRow[]
}

export interface ITableHeader {
    accessor: string;
    title: string;
    order?: number;
}

export interface ITableRow {
    [x: string]: string | number | boolean | Date;
}
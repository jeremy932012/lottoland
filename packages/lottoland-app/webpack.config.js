const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
  entry: './src/App.tsx',
  target: 'web',
  devtool: "source-map",
  mode: "development",
  resolve: {
      extensions: ['.ts', '.tsx', '.js'],
      alias: {
        react: path.resolve('./node_modules/react')
      }
  },
  module: {
      rules: [
          {
              test: /\.tsx?$/,
              use: 'ts-loader',
              exclude: '/node_modules/'
          },
          {
            test: /\.s(a|c)ss$/,
            use: [
              'style-loader',
              {
                loader: 'css-loader',
                options: { modules: true }
              },
              'sass-loader'
            ]
          }
      ],
  },
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
  },
  plugins: [new HtmlWebpackPlugin({
      template: "./public/index.html",
  })]
};
import React from "react";
import ReactDOM from "react-dom";
import { Lottoland } from "./Lottoland";

ReactDOM.render(<Lottoland />, document.getElementById("app"));
interface ILottolandData {
    last: ILottolandItem[]
}

interface ILottolandItem {
    jackpot: number;
    Winners: number;
    numbers: number[];
    euroNumbers: number[];
}
import React, { useEffect, useState } from "react";
import { Header } from "lottoland-header";
import { Dropdown } from "lottoland-dropdown";
import { Table } from "lottoland-table";
import "./Lottoland.scss"
import { IDropdownItem } from "lottoland-dropdown/build/DropdownDefinition";
import { ITableHeader, ITableRow } from "lottoland-table/build/TableDefinition";

const BYPASS_CORS = "https://cors-anywhere.herokuapp.com/";
const LOTTOLAND_URL = "https://www.lottoland.com/api/drawings/euroJackpot";

export function Lottoland() {
    const [dropdownItems, setDropdownItems] = useState<IDropdownItem[]>([]);
    const [tableData, setTableData] = useState<ITableRow[]>([]);

    const tableHeader: ITableHeader[] = [
        {accessor: "tier", title: "Tier"}, 
        {accessor: "match", title: "Match"}, 
        {accessor: "winners", title: "Winners"}, 
        {accessor: "jackpot", title: "Amount"}
    ];

    useEffect(() => {
        fetch(`${BYPASS_CORS}${LOTTOLAND_URL}`).then(res => res.json()).then(data => {
            setDropdownItems(Object.keys(data).map(key => ({key, value: data[key].drawingDate})))
        })
    },[])

    const onItemClick = (item: IDropdownItem) => {
        const dateValue = item.value.split(",")[0];
        const dateValueQuery = dateValue.split(".").reverse().join("");
        fetch(`${BYPASS_CORS}${LOTTOLAND_URL}/${dateValueQuery}`).then(res => res.json()).then(data => {
            setTableData(data.last.map((loto: ILottolandItem, i: number) => ({
                tier: i,
                jackpot: "€ " + new Intl.NumberFormat("en-GB").format(loto.jackpot),
                winners: new Intl.NumberFormat("en-GB").format(loto.Winners)+"x",
                match: loto.numbers.length + " Numbers + " + loto.euroNumbers.length + " Euronumbers"
            })));
        })
    }


    return (
        <div className="lottoland--container">
            <Header title={"Lottoland API Tester"}></Header>
            <Dropdown onItemClick={onItemClick} items={dropdownItems} placeholder="Hola"></Dropdown>
            {tableData.length > 0  && <Table header={tableHeader} data={tableData}></Table>}          
        </div>
    );
}